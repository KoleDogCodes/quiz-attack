import os

counter = 0
xml_counter = 0

def getListOfFiles(dirName):
    # create a list of file and sub directories 
    # names in the given directory 
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
                
    return allFiles

def readFile(path):
    f = open(path, "r")
    return (path.split('\\')[-1], len(f.readlines()))

listOfFiles = getListOfFiles("C:\\Users\\Kolade\\Desktop\\Quiz Attack Application\\app\\src\\main")

for f in listOfFiles:
    if f.endswith(".java"):
        stat = readFile(f)
        counter += stat[1]
        print(stat)

    elif f.endswith(".xml"):
        stat = readFile(f)
        xml_counter += stat[1]

print("--------------------------------------")
print("Java Code Line Numbers: " + str(counter))
print("XML Code Line Numbers: " + str(xml_counter))
print("Java & XML Code Line Numbers: " + str(counter + xml_counter))
print("--------------------------------------")
