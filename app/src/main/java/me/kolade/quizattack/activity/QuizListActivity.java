package me.kolade.quizattack.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import me.kolade.quizattack.R;
import me.kolade.quizattack.adapter.QuizIconItem;
import me.kolade.quizattack.adapter.QuizIconItemAdapter;
import me.kolade.quizattack.adapter.TopicItem;
import me.kolade.quizattack.adapter.TopicItemAdapter;
import me.kolade.quizattack.listeners.ActivityListeners;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.LayoutUtility;
import me.kolade.quizattack.utility.QuizUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class QuizListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        SharedPreferencesWrapper sp = new SharedPreferencesWrapper(this);

        //Set navbar selected item
        BottomNavigationView navBar = findViewById(R.id.navBottom);
        navBar.setSelectedItemId(R.id.menuItemQuizList);

        //Start navbar listener
        ActivityListeners.bottomNavBarSelectListener(this);

        final String[] topics = QuizUtility.getTopics(this);

        //Setup recycler viewer (GRID LAYOUT)
        final List<QuizIconItem> itemList = new ArrayList<QuizIconItem>();
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final QuizIconItemAdapter adapter = new QuizIconItemAdapter(this, itemList, new QuizIconItemAdapter.InnerClickListener() {
            @Override
            public void onClick(View v, String quizId, int position) {
                ActivityUtility.startActivity(QuizListActivity.this, QuizEditorActivity.class, "quiz_id", quizId);
            }
        });

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 5));
        //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, GridLayoutManager.HORIZONTAL, false);
        //recyclerView.setLayoutManager(layoutManager);

        //Show loading overlay
        LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), true);

        //Get all quizzes
        ActivityUtility.runAsyncTask(new Runnable() {
            @Override
            public void run() {
                List<QuizIconItem> items = QuizUtility.getCreatedQuizzes(QuizListActivity.this);
                itemList.addAll(items);

                //Run sync task
                ActivityUtility.runSyncTask(new ActivityUtility.SyncTask() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });

                LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), false);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            ActivityUtility.startActivity(QuizListActivity.this, MainActivity.class);
            finish();
        }

        return false;
    }

}

