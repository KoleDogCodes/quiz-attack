package me.kolade.quizattack.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.kolade.quizattack.R;
import me.kolade.quizattack.adapter.SettingItem;
import me.kolade.quizattack.adapter.SettingsListAdapter;
import me.kolade.quizattack.adapter.TopicItem;
import me.kolade.quizattack.adapter.TopicItemAdapter;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.handler.WebManager;
import me.kolade.quizattack.listeners.ActivityListeners;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.LayoutUtility;
import me.kolade.quizattack.utility.QuizUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class FeedPostActivity extends AppCompatActivity {

    private int SELECTED_TOPIC = 0;
    private String PICTURE = "";
    private SharedPreferencesWrapper sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        sp = new SharedPreferencesWrapper(this);

        //Chnage post header
        TextView lblHeader2 = findViewById(R.id.lblHeader2);
        Spanned formattedText = Html.fromHtml(User.getLocalDisplayName(this) + " to <font color=\"blue\"><b>" + QuizUtility.getTopics(this)[SELECTED_TOPIC] + "</b></font>");
        lblHeader2.setText(formattedText);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            ActivityUtility.startActivity(FeedPostActivity.this, FeedActivity.class);
            finish();
        }

        return false;
    }

    public void onCancel(View view) {
        ActivityUtility.startActivity(FeedPostActivity.this, FeedActivity.class);
        finish();
    }

    public void onPost(View view) {
        //Get elements
        EditText txtContent = findViewById(R.id.txtPost);
        LayoutUtility.toggleActivityLayout(findViewById(R.id.constraintLayout), false);
        LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), true);

        //Set parameter for post request
        Map<String, String> params = new HashMap<>();
        params.put("key", User.getLocalApiKey(this));
        params.put("topicId", String.valueOf(SELECTED_TOPIC));
        params.put("image", PICTURE);
        params.put("content", txtContent.getText().toString());

        WebManager.request(WebManager.API_URL + "api/feed", WebManager.RequestType.POST, params, new WebManager.RequestListener() {
            @Override
            public void onResponse(WebManager.ResponseObject response) {
            if (response.getBoolean("success")) {
                ActivityUtility.toast(FeedPostActivity.this, response.getString("message"), 45);
                ActivityUtility.startActivity(FeedPostActivity.this, FeedActivity.class);
            }
            }
        });
    }

    public void onChangeTopic(View view) {
        try {
            //Select topic
            ActivityUtility.showDialog(this, "Select a topic", QuizUtility.getTopics(this), new ActivityUtility.DialogListener() {
                @Override
                public void onClick(DialogInterface dialog, int position) {
                    SELECTED_TOPIC = position;
                    TextView lblHeader2 = findViewById(R.id.lblHeader2);
                    Spanned formattedText = Html.fromHtml(User.getLocalDisplayName(FeedPostActivity.this) + " to <font color=\"blue\"><b>" + QuizUtility.getTopics(FeedPostActivity.this)[SELECTED_TOPIC] + "</b></font>");
                    lblHeader2.setText(formattedText);
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPreviewClick(View view) {
        ActivityUtility.showDialog(FeedPostActivity.this, "Delete Picture", new String[]{"Are you sure?"}, new ActivityUtility.DialogListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                if (position == 0) {
                    PICTURE = "";
                    ImageView imgTranslate = findViewById(R.id.imgTranslate);
                    imgTranslate.setVisibility(View.GONE);
                }
            }
        });
    }

    public void onAddImage(View view) {
        selectPicture();
    }

    private void selectPicture() {
        ActivityUtility.showDialog(FeedPostActivity.this, "Select Photo", new String[]{"Take a photo", "Select from gallery"}, new ActivityUtility.DialogListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                if (position == 0) {
                    Intent intentTakePic = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intentTakePic, 0);
                }
                else if (position == 1) {
                    Intent intentSelectPic = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intentSelectPic , 1);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ImageView imgTranslate = findViewById(R.id.imgTranslate);

        //User took picture
        if (resultCode == RESULT_OK && requestCode == 0) {
            Bitmap img = (Bitmap) data.getExtras().get("data");
            PICTURE = LayoutUtility.encodeBitmap(img);
            imgTranslate.setImageBitmap(img);
            imgTranslate.setVisibility(View.VISIBLE);
        }

        //User selected picture from local storage
        else if (resultCode == RESULT_OK && requestCode == 1 && data != null) {
            Uri selectedImage = data.getData();

            //Hacky method to get image as bitmap
            imgTranslate.setImageURI(selectedImage);
            BitmapDrawable bitmapDrawable = (BitmapDrawable) imgTranslate.getDrawable();
            Bitmap bmp = bitmapDrawable.getBitmap();
            PICTURE = LayoutUtility.encodeBitmap(bmp);
            imgTranslate.setVisibility(View.VISIBLE);
        }

    }

}

