package me.kolade.quizattack.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.kolade.quizattack.R;
import me.kolade.quizattack.adapter.QuestionItem;
import me.kolade.quizattack.adapter.QuestionsItemAdapter;
import me.kolade.quizattack.adapter.SettingItem;
import me.kolade.quizattack.adapter.SettingsListAdapter;
import me.kolade.quizattack.handler.InputDialog;
import me.kolade.quizattack.handler.Quiz;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.handler.WebManager;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.LayoutUtility;
import me.kolade.quizattack.utility.QuizUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class QuizEditorActivity extends AppCompatActivity {

    private int SELECTED_TOPIC = 0, LAST_POSITION = 0;
    private String TITLE = "";
    private boolean DISABLE_TOUCH = false;
    private Bitmap ICON = null;

    private Spanned getFormattedText(String prefix, String value) {
        Spanned formattedText = Html.fromHtml(prefix + "<font color=\"blue\"><b>" + value + "</b></font>");
        return formattedText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quiz);

        //Set sub headers
        TextView lblTopic = findViewById(R.id.lblTopic);
        lblTopic.setText(getFormattedText("Topic: ", QuizUtility.getTopics(this)[SELECTED_TOPIC]));

        //Generate question list
        final List<QuestionItem> items = new ArrayList<QuestionItem>();

        //Setup recycler viewer
        final RecyclerView recyclerView = findViewById(R.id.recyclerView);

        //Pass event data to "callback" function
        QuestionsItemAdapter adapter = new QuestionsItemAdapter(this, items, new QuestionsItemAdapter.ClickListener() {
            @Override
            public void onClick(QuestionsItemAdapter.EventType type, View v, int clickedId, int position) {
                LAST_POSITION = position;
                onEditorEvent(type, v, clickedId, position);
            }
        });

        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        //If quiz id is intent'ed from quiz list screen
        if (getIntent().hasExtra("quiz_id")) {
            DISABLE_TOUCH = true;
            LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), true);

            final String quizId = getIntent().getStringExtra("quiz_id");

            ActivityUtility.runAsyncTask(new Runnable() {
                @Override
                public void run() {
                    String[] topics = QuizUtility.getTopics(QuizEditorActivity.this);
                    Quiz quiz = QuizUtility.getQuiz(QuizEditorActivity.this, quizId);

                    //Get all the views
                    TextView lblTitle = findViewById(R.id.lblTitle);
                    TextView lblTopic = findViewById(R.id.lblTopic);
                    ImageView imgIcon = findViewById(R.id.imgIcon);
                    Switch btnPrivacy = findViewById(R.id.btnPrivacy);
                    Button btnUpdateQuiz = findViewById(R.id.btnSave);

                    TITLE = quiz.getTitle();
                    SELECTED_TOPIC = Integer.valueOf(quiz.getTopicId());
                    ICON = quiz.getIcon();

                    //Set the text views
                    LayoutUtility.runViewMethod(lblTitle, "setText", getFormattedText("Quiz Title: ", quiz.getTitle()));
                    LayoutUtility.runViewMethod(lblTopic, "setText", getFormattedText("Topic: ", topics[Integer.valueOf(quiz.getTopicId())]));
                    LayoutUtility.runViewMethod(imgIcon, "setImageBitmap", quiz.getIcon());
                    LayoutUtility.runViewMethod(btnUpdateQuiz, "setText", "UPDATE QUIZ");
                    LayoutUtility.runViewMethod(btnUpdateQuiz, "setTag", "UPDATE_STATE");

                    //Add questions and update recycler view
                    items.addAll(quiz.getQuestions());
                    LayoutUtility.runMethod(RecyclerView.Adapter.class, recyclerView.getAdapter(), "notifyDataSetChanged", null);

                    DISABLE_TOUCH = false;
                    LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), false);
                }
            });

            return;
        }

        for (int i = 0; i < 10; i++) {
            items.add(new QuestionItem(i, "", null, 0));
        }

        //Alert user to saving question as they go
        ActivityUtility.alertBox(this, "NOTE: Save question as you go.");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        //User took picture
        if (resultCode == RESULT_OK && requestCode == 0) {
            Bitmap img = (Bitmap) data.getExtras().get("data");

            //Save the question text with no changes in correct question
            save(LAST_POSITION, -1, img);
        }

        //User selected picture from local storage for question
        else if (resultCode == RESULT_OK && requestCode == 1 && data != null) {
            Uri selectedImage = data.getData();

            //Hacky method to get image as bitmap
            ImageView convert = findViewById(R.id.imgTranslate);
            convert.setImageURI(selectedImage);
            BitmapDrawable bitmapDrawable = (BitmapDrawable) convert.getDrawable();
            Bitmap img = bitmapDrawable.getBitmap();

            //Save the question text with no changes in correct question
            save(LAST_POSITION, -1, img);
        }

        //User selected picture from local storage for quiz icon
        else if (resultCode == RESULT_OK && requestCode == 2 && data != null) {
            Uri selectedImage = data.getData();

            //Hacky method to get image as bitmap
            ImageView convert = findViewById(R.id.imgTranslate);
            convert.setImageURI(selectedImage);
            BitmapDrawable bitmapDrawable = (BitmapDrawable) convert.getDrawable();
            ICON = bitmapDrawable.getBitmap();

            ImageView imgIcon = findViewById(R.id.imgIcon);
            imgIcon.setImageDrawable(bitmapDrawable);
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        if (DISABLE_TOUCH){ return true; }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            ActivityUtility.startActivity(QuizEditorActivity.this, QuizListActivity.class);
        }

        return false;
    }

    private void selectQuestionPicture() {
        ActivityUtility.showDialog(this, "Select Photo", new String[]{"Take a photo", "Select from gallery"}, new ActivityUtility.DialogListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                if (position == 0) {
                    Intent intentTakePic = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intentTakePic, 0);
                }
                else if (position == 1) {
                    Intent intentSelectPic = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intentSelectPic , 1);
                }
            }
        });
    }

    private void selectIconPicture() {
        ActivityUtility.showDialog(this, "Select Photo", new String[]{"Select from gallery"}, new ActivityUtility.DialogListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                if (position == 0) {
                    Intent intentSelectPic = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intentSelectPic , 2);
                }

            }
        });
    }

    private void save(int position, int answer, Bitmap img) {
        final RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final QuestionsItemAdapter adapter = (QuestionsItemAdapter) recyclerView.getAdapter();

        View rowView = recyclerView.findViewHolderForAdapterPosition(position).itemView;

        ImageView btnSave = rowView.findViewById(R.id.btnSave);
        btnSave.setImageResource(R.drawable.ic_save_green_24dp);

        //Get text fields and clear focus to prevent further issues
        EditText txtQuestion = rowView.findViewById(R.id.txtQuestion);
        EditText txtOption1 = rowView.findViewById(R.id.txtOption1);
        EditText txtOption2 = rowView.findViewById(R.id.txtOption2);
        EditText txtOption3 = rowView.findViewById(R.id.txtOption3);
        EditText txtOption4 = rowView.findViewById(R.id.txtOption4);
        txtQuestion.clearFocus();
        txtOption1.clearFocus();
        txtOption2.clearFocus();
        txtOption3.clearFocus();
        txtOption4.clearFocus();

        String[] options = new String[4];
        String question = txtQuestion.getText().toString();
        options[0] = txtOption1.getText().toString();
        options[1] = txtOption2.getText().toString();
        options[2] = txtOption3.getText().toString();
        options[3] = txtOption4.getText().toString();

        QuestionItem oldItem = adapter.getItem(position);
        adapter.setItem(position,
                new QuestionItem(position,
                question,
                options,
                answer == -1 ? oldItem.getCorrectQuestion(): answer)
                .setImage(img == null ? oldItem.getQuestionImage() : img
        ));
    }

    private void onEditorEvent(QuestionsItemAdapter.EventType type, View rowView, int clickedId, int position) {
        final RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final QuestionsItemAdapter adapter = (QuestionsItemAdapter) recyclerView.getAdapter();

        //De-select all check boxes if event type == SELECT ANSWER
        if (type == QuestionsItemAdapter.EventType.SELECT_ANSWER) {
            Resources res = getResources();
            for (int i = 0; i < 4; i++) {
                int id = res.getIdentifier("chkOption" + (i + 1), "id", getPackageName());
                final CheckBox checkBox = rowView.findViewById(id);

                if (checkBox.getId() == clickedId) {
                    save(LAST_POSITION, i, null);
                    continue;
                }

                checkBox.setChecked(false);
            }
        }

        //Start picture selection intent
        else if (type == QuestionsItemAdapter.EventType.SELECT_PICTURE) {
            selectQuestionPicture();
        }

        //User clicked on image
        else if (type == QuestionsItemAdapter.EventType.PICTURE_CLICKED) {
            ActivityUtility.showDialog(this, "Delete Picture", new String[]{"Are you sure?"}, new ActivityUtility.DialogListener() {
                @Override
                public void onClick(DialogInterface dialog, int position) {
                    if (position == 0) {
                        ImageView imgContent = recyclerView.findViewHolderForAdapterPosition(LAST_POSITION).itemView.findViewById(R.id.imgContent);
                        imgContent.setImageBitmap(null);
                        imgContent.setVisibility(View.GONE);

                        //Save the question text with no changes in correct question or image
                        save(LAST_POSITION, -1, null);
                        QuestionItem oldItem = adapter.getItem(LAST_POSITION);
                        adapter.setItem(LAST_POSITION, new QuestionItem(oldItem.getQuestionNumber(), oldItem.getQuestion(), oldItem.getOptions(), oldItem.getCorrectQuestion()));
                    }
                }
            });
        }

        //User has clicked save button
        else if (type == QuestionsItemAdapter.EventType.SAVE_QUESTION) {
            //Save the question text with no changes in correct question or image
            save(LAST_POSITION, -1, null);
        }
    }

    public void onIconSelect(View view) {
        selectIconPicture();
    }

    public void onEditTitle(View view) {
        ActivityUtility.showDialog(this, "Input Quiz Title", new InputDialog.DialogResult() {
            @Override
            public void onSuccess(String result) {
                if (result.length() > 30) {
                    ActivityUtility.alertBox(QuizEditorActivity.this, "Quiz title must be less than 30 characters.");
                    return;
                }

                TITLE = result;
                TextView lblTitle = findViewById(R.id.lblTitle);
                Spanned formattedText = Html.fromHtml("Quiz Title: <font color=\"blue\"><b>" +TITLE + "</b></font>");
                lblTitle.setText(formattedText);
            }
        });
    }

    public void onEditTopic(View view) {

        try {
            //Select topic
            ActivityUtility.showDialog(this, "Select a topic", QuizUtility.getTopics(this), new ActivityUtility.DialogListener() {
                @Override
                public void onClick(DialogInterface dialog, int position) {
                    SELECTED_TOPIC = position;
                    TextView lblTopic = findViewById(R.id.lblTopic);
                    Spanned formattedText = Html.fromHtml("Topic: <font color=\"blue\"><b>" + QuizUtility.getTopics(QuizEditorActivity.this)[SELECTED_TOPIC] + "</b></font>");
                    lblTopic.setText(formattedText);
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSave(final View view) {
        final JSONArray quizData = new JSONArray();
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final QuestionsItemAdapter adapter = (QuestionsItemAdapter) recyclerView.getAdapter();
        Switch btnPrivacy = findViewById(R.id.btnPrivacy);

        //Basic Validation
        if (TITLE.trim().equals("")) {
            ActivityUtility.alertBox(this, "Please input a valid quiz title.");
            return;
        }

        if (ICON == null) {
            ActivityUtility.alertBox(this, "Please select an icon for your quiz.");
            return;
        }

        //Check if all questions are saved
        boolean isAllSaved = true;
        for (int i = 0; i < adapter.getItemCount(); i++) {
            if (!adapter.isSaved(i)) {
                ActivityUtility.alertBox(QuizEditorActivity.this, "Question " + (i + 1) + " must be saved before continuing.");
                isAllSaved = false;
                break;
            }
        }

        if (!isAllSaved) { return; }

        //Put app into save state
        LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), true);
        DISABLE_TOUCH = true;

        //Set params for request
        final Map<String, String> params = new HashMap<String, String>();
        params.put("key", User.getLocalApiKey(this));
        params.put("title", TITLE);
        params.put("topicId", String.valueOf(SELECTED_TOPIC));
        params.put("status", String.valueOf(btnPrivacy.isChecked()));

        //Run quiz serialization in seperate thread to avoid blocking main thread
        ActivityUtility.runAsyncTask(new Runnable() {
            @Override
            public void run() {
                try {
                    params.put("icon", LayoutUtility.encodeBitmap(ICON));

                    //Parse into json data
                    for (int i = 0; i < adapter.getItemCount(); i++) {
                        QuestionItem item = adapter.getItem(i);
                        JSONObject questionData = new JSONObject();
                        questionData.put("question", item.getQuestion());
                        questionData.put("image", LayoutUtility.encodeBitmap(item.getQuestionImage()));
                        questionData.put("options", new JSONArray(item.getOptions()).toString());
                        questionData.put("answer", item.getCorrectQuestion());

                        quizData.put(questionData);
                    }

                    params.put("metadata", quizData.toString());
                    params.put("icon", LayoutUtility.encodeBitmap(ICON));

                    //Update request type to send quiz data to added to database
                    WebManager.RequestType requestType = WebManager.RequestType.POST;

                    //Update request type to send quiz data to be updated
                    if (view.getTag() != null){
                        params.put("id", getIntent().getStringExtra("quiz_id"));
                        requestType = WebManager.RequestType.PUT;
                    }

                    //Send request to create quiz in the database
                    WebManager.request(WebManager.API_URL + "api/quiz", requestType, params, new WebManager.RequestListener() {
                        @Override
                        public void onResponse(WebManager.ResponseObject response) {
                            ActivityUtility.toast(QuizEditorActivity.this, response.getString("message"), 45);

                            DISABLE_TOUCH = false;
                            LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), false);
                        }
                    });

                }
                catch (Exception e) {
                    ActivityUtility.alertBox(QuizEditorActivity.this, "ERROR: Failed to create the quiz.");
                    e.printStackTrace();
                }
            }
        });
    }

    public void onAddQuestion(View view) {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        QuestionsItemAdapter adapter = (QuestionsItemAdapter) recyclerView.getAdapter();
        adapter.addItem();
        ActivityUtility.toast(this, "Question " + adapter.getItemCount() + " was added.", 45);
    }

    public void onRemoveQuestion(View view) {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        QuestionsItemAdapter adapter = (QuestionsItemAdapter) recyclerView.getAdapter();

        //Validation
        if (adapter.getItemCount() <= 3) {
            ActivityUtility.toast(this, "You must have at least 3 questions.", 45);
            return;
        }

        adapter.removeItem(adapter.getItemCount() - 1);
        ActivityUtility.toast(this, "Question " + (adapter.getItemCount() + 1) + " was removed.", 45);
    }
}

