package me.kolade.quizattack.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.kolade.quizattack.R;
import me.kolade.quizattack.adapter.QuestionResultItem;
import me.kolade.quizattack.adapter.QuestionResultsAdapter;
import me.kolade.quizattack.adapter.SettingsListAdapter;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class QuizResultsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_result);

        int score = getIntent().getIntExtra("score", 0);
        HashMap<Integer, Boolean> results = (HashMap<Integer, Boolean>) getIntent().getSerializableExtra("results");
        List<QuestionResultItem> items = new ArrayList<>();

        ListView lstResults = findViewById(R.id.lstResults);
        QuestionResultsAdapter adapter = new QuestionResultsAdapter(items, this, R.id.lstResults);
        TextView lblScore = findViewById(R.id.lblScore);

        lblScore.setText("Your Score: " + score);
        lstResults.setAdapter(adapter);

        //Generate list view with all correct and incorrect answers
        for (Map.Entry entry : results.entrySet()) {
            QuestionResultItem itemResult = new QuestionResultItem(Integer.valueOf(entry.getKey().toString()), Boolean.valueOf(entry.getValue().toString()));
            items.add(itemResult);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { }

        return false;
    }

    public void onGoBack(View view) {
        ActivityUtility.startActivity(this, TopicActivity.class);
    }

}

