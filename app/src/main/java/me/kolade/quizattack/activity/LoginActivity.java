package me.kolade.quizattack.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import me.kolade.quizattack.R;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.handler.WebManager;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.LayoutUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;


public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
            ActivityUtility.startActivity(this, WelcomeActivity.class);
            finish();
        }

        return false;
    }

    public void onLogin(View view) {
        //Enables layout
        LayoutUtility.toggleActivityLayout((ConstraintLayout) findViewById(R.id.layout), false);

        //Show progress bar overlay
        LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), true);

        //Get email and password from text fields
        EditText txtEmail = findViewById(R.id.txtEmail);
        EditText txtPassword = findViewById(R.id.txtPassword);
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();

        //Authenticate user
        WebManager.requestGet(WebManager.API_URL + "api/account?email=" + email + "&password=" + password, new WebManager.RequestListener() {
            @Override
            public void onResponse(WebManager.ResponseObject response) {
                if (response.getBoolean("success")) {
                    //Save users api key locally
                    SharedPreferencesWrapper sp = new SharedPreferencesWrapper(LoginActivity.this);
                    sp.addValue("apiKey", response.getString("key"));
                    sp.addValue("userId", response.getString("id"));

                    //Start main activity
                    ActivityUtility.startActivity(LoginActivity.this, MainActivity.class);
                    finish();
                    return;
                }

                //Enables layout
                LayoutUtility.toggleActivityLayout((ConstraintLayout) findViewById(R.id.layout), true);

                //Show progress bar overlay
                LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), false);

                //Show error message
                ActivityUtility.toast(LoginActivity.this, response.getString("message"), 45);
            }
        });
    }
}
