package me.kolade.quizattack.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import me.kolade.quizattack.R;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        SharedPreferencesWrapper sp = new SharedPreferencesWrapper(this);

        if (sp.hasKey("apiKey")) {
            ActivityUtility.startActivity(this, MainActivity.class);
            finish();
            return;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

        }

        return false;
    }

    public void onLoginClick(View view) {
        ActivityUtility.startActivity(this, LoginActivity.class);
        finish();
    }

    public void onJoinClick(View view) {
        ActivityUtility.startActivity(this, RegisterActivity.class);
        finish();
    }
}

