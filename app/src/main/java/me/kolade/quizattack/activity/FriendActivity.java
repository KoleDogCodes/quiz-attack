package me.kolade.quizattack.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import me.kolade.quizattack.R;
import me.kolade.quizattack.handler.SocketManager;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.listeners.ActivityListeners;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class FriendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        SharedPreferencesWrapper sp = new SharedPreferencesWrapper(this);

        //Set navbar selected item
        BottomNavigationView navBar = findViewById(R.id.navBottom);
        navBar.setSelectedItemId(R.id.menuItemFriends);

        //Start navbar listener
        ActivityListeners.bottomNavBarSelectListener(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            ActivityUtility.startActivity(FriendActivity.this, MainActivity.class);
            finish();
        }

        return false;
    }

    public void onClick(View view) {
        JSONObject obj = new JSONObject();

        try {
            obj.put("name", User.getLocalDisplayName(this));
            SocketManager.sendMessage("friend", obj);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

