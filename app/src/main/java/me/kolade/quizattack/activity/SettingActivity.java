package me.kolade.quizattack.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import me.kolade.quizattack.R;
import me.kolade.quizattack.adapter.SettingItem;
import me.kolade.quizattack.adapter.SettingsListAdapter;
import me.kolade.quizattack.listeners.SocketService;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //App Settings List View
        List<SettingItem> appItems = new ArrayList<SettingItem>();
        appItems.add(new SettingItem(R.drawable.ic_notifications_blue_24dp, "Push Notifications", SettingItem.Type.SWITCH).checked(true));

        ListView lstAppSettings = findViewById(R.id.lstAppSettings);
        SettingsListAdapter appSettingsApdapter = new SettingsListAdapter(appItems, this, R.id.lstAppSettings);
        lstAppSettings.setAdapter(appSettingsApdapter);

        //Account Settings List View
        List<SettingItem> accountItems = new ArrayList<SettingItem>();
        accountItems.add(new SettingItem(R.drawable.ic_person_outline_pink_24dp, "Edit Profile", SettingItem.Type.NEXT));
        accountItems.add(new SettingItem(R.drawable.ic_exit_to_app_black_24dp, "Logout", SettingItem.Type.NONE));

        ListView lstAccountSettings = findViewById(R.id.lstAccountSettings);
        SettingsListAdapter accountSettingsApdapter = new SettingsListAdapter(accountItems, this, R.id.lstAccountSettings);
        lstAccountSettings.setAdapter(accountSettingsApdapter);

        //Account Settings Event
        lstAccountSettings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                //Profile Edit
                if (position == 0) {
                    ActivityUtility.startActivity(SettingActivity.this, EditProfileActivity.class);
                    finish();
                }

                //Logout
                else if (position == 1) {
                    SharedPreferencesWrapper sp = new SharedPreferencesWrapper(SettingActivity.this);
                    sp.clear();
                    ActivityUtility.startActivity(SettingActivity.this, WelcomeActivity.class);
                    finish();
                    stopService(new Intent(SettingActivity.this, SocketService.class));
                }
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            ActivityUtility.startActivity(SettingActivity.this, MainActivity.class);
            finish();
        }

        return false;
    }

}

