package me.kolade.quizattack.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.kolade.quizattack.R;
import me.kolade.quizattack.adapter.FeedItem;
import me.kolade.quizattack.adapter.FeedItemAdapter;
import me.kolade.quizattack.adapter.QuizIconItem;
import me.kolade.quizattack.adapter.TopicItem;
import me.kolade.quizattack.adapter.TopicItemAdapter;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.handler.WebManager;
import me.kolade.quizattack.listeners.ActivityListeners;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.LayoutUtility;
import me.kolade.quizattack.utility.NotificationUtility;
import me.kolade.quizattack.utility.QuizUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class TopicActivity extends AppCompatActivity {

    private SharedPreferencesWrapper sp;
    private boolean DISABLE_TOUCH = false;

    public void setupRecyclers() {
        final String[] topics = QuizUtility.getTopics(TopicActivity.this);

        //Setup primary recycler viewer
        final RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final List<TopicItem> topicItemList = new ArrayList<TopicItem>();

        TopicItemAdapter apt = new TopicItemAdapter(TopicActivity.this, topicItemList, new TopicItemAdapter.ClickListener() {
            @Override
            public void onClick(View v, String quizId, int position) {
                onTopicClick(v, quizId, position);
            }
        });

        recyclerView.setAdapter(apt);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(TopicActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        //Retrieve quizzes in different thread and display them
        ActivityUtility.runAsyncTask(new Runnable() {
            @Override
            public void run() {
                try {
                    //Populate primary and sub recycle viewer
                    for (int i = 0; i < topics.length; i++) {
                        List<QuizIconItem> items = QuizUtility.getQuizzes(TopicActivity.this, i);
                        topicItemList.add(new TopicItem(topics[i], items));
                    }

                    DISABLE_TOUCH = false;
                    LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), false);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topics);

        sp = new SharedPreferencesWrapper(this);

        //Set navbar selected item
        BottomNavigationView navBar = findViewById(R.id.navBottom);
        navBar.setSelectedItemId(R.id.menuItemQuiz);

        //Start navbar listener
        ActivityListeners.bottomNavBarSelectListener(this);

        //Download topics if missing
        DISABLE_TOUCH = true;
        LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), true);

        if (!sp.hasKey("topics")) {
            WebManager.requestGet(WebManager.API_URL + "api/topics?key=" + User.getLocalApiKey(this), new WebManager.RequestListener() {
                @Override
                public void onResponse(WebManager.ResponseObject response) {
                    if (response.getBoolean("success")) {
                        sp.addValue("topics", response.getString("topics"));
                        NotificationUtility.sendNotification(TopicActivity.this, "QuizAttack", "Quiz topics have been successfully downloaded.");
                        setupRecyclers();
                        return;
                    }

                    NotificationUtility.sendNotification(TopicActivity.this, "QuizAttack", "Failed to download topics.");
                }
            });

            return;
        }

        setupRecyclers();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        if (DISABLE_TOUCH){ return true; }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            ActivityUtility.startActivity(TopicActivity.this, MainActivity.class);
            finish();
        }

        return false;
    }

    public void onCreateQuiz(View view) {
        ActivityUtility.startActivity(this, QuizEditorActivity.class);
    }

    public void onTopicClick(View v, String quizId, int position) {
        TextView tv = v.findViewById(R.id.lblTitle);
        ActivityUtility.startActivity(TopicActivity.this, QuizGameActivity.class, "quizId", quizId);
    }

}

