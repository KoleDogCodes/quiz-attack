package me.kolade.quizattack.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.kolade.quizattack.R;
import me.kolade.quizattack.adapter.SettingItem;
import me.kolade.quizattack.adapter.SettingsListAdapter;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.handler.WebManager;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.LayoutUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class EditProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        //Profile Settings List View
        SharedPreferencesWrapper sp = new SharedPreferencesWrapper(this);
        List<SettingItem> profileItems = new ArrayList<SettingItem>();

        //Option #1 - Update Profile Picture
        if (sp.hasKey("profileImage")){
            profileItems.add(new SettingItem(R.drawable.ic_forum_black_24dp, "Profile Pic", SettingItem.Type.NEXT).setImage(LayoutUtility.decodeBase64(sp.getString("profileImage"))));
        }
        else {
            profileItems.add(new SettingItem(R.drawable.ic_forum_black_24dp, "Profile Pic", SettingItem.Type.NEXT));
        }

        //Option 2 - Update Display name
        profileItems.add(new SettingItem(R.drawable.ic_supervisor_account_pink_24dp, "New Display Name", SettingItem.Type.TEXT_FIELD).setText(User.getLocalDisplayName(this)));

        //Set list adapter to custom list
        ListView lstAccountSettings = findViewById(R.id.lstEditProfile);
        SettingsListAdapter appSettingsApdapter = new SettingsListAdapter(profileItems, this, R.id.lstEditProfile);
        lstAccountSettings.setAdapter(appSettingsApdapter);

        //Account Settings Event
        lstAccountSettings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                //Change Profile Picture
                if (position == 0) {
                    selectPicture();
                }
            }
        });
    }

    private void selectPicture() {
        ActivityUtility.showDialog(EditProfileActivity.this, "Select Photo", new String[]{"Take a photo", "Select from gallery"}, new ActivityUtility.DialogListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                if (position == 0) {
                    Intent intentTakePic = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intentTakePic, 0);
                }
                else if (position == 1) {
                    Intent intentSelectPic = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intentSelectPic , 1);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //User took picture
        if (resultCode == RESULT_OK && requestCode == 0) {
            Bitmap img = (Bitmap) data.getExtras().get("data");

            //Update profile picture locally
            ListView lstEditProfile = findViewById(R.id.lstEditProfile);
            SettingsListAdapter adapter = (SettingsListAdapter) lstEditProfile.getAdapter();
            adapter.remove((SettingItem) lstEditProfile.getAdapter().getItem(0));
            adapter.insert(new SettingItem("Profile Pic", SettingItem.Type.NEXT).setImage(img), 0);

            //Store image encoded as base64 string
            SharedPreferencesWrapper sp = new SharedPreferencesWrapper(EditProfileActivity.this);
            sp.addValue("profileImage", LayoutUtility.encodeBitmap(img));
        }

        //User selected picture from local storage
        else if (resultCode == RESULT_OK && requestCode == 1 && data != null) {
            Uri selectedImage = data.getData();

            //Hacky method to get image as bitmap
            ImageView convert = findViewById(R.id.imgTranslate);
            convert.setImageURI(selectedImage);
            BitmapDrawable bitmapDrawable = (BitmapDrawable) convert.getDrawable();
            Bitmap bmp = bitmapDrawable.getBitmap();

            //Store image encoded as base64 string
            SharedPreferencesWrapper sp = new SharedPreferencesWrapper(EditProfileActivity.this);
            sp.addValue("profileImage", LayoutUtility.encodeBitmap(bmp));

            //Update profile picture in list view
            ListView lstEditProfile = findViewById(R.id.lstEditProfile);
            SettingsListAdapter adapter = (SettingsListAdapter) lstEditProfile.getAdapter();
            adapter.remove((SettingItem) lstEditProfile.getAdapter().getItem(0));
            adapter.insert(new SettingItem("Profile Pic", SettingItem.Type.NEXT).setImage(bmp), 0);
            lstEditProfile.setAdapter(adapter);
        }

    }

    public void onSaveProfile(View view) {
        LayoutUtility.toggleActivityLayout(findViewById(R.id.constraintLayout), false);
        LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), true);

        SharedPreferencesWrapper sp = new SharedPreferencesWrapper(EditProfileActivity.this);

        //Get display name input from listview
        ListView lstEditProfile = findViewById(R.id.lstEditProfile);
        View firstItemView = lstEditProfile.getChildAt(1);

        String displayName = ((EditText) firstItemView.findViewById(R.id.txtRowInput)).getText().toString();

        //Set parameters for PUT request to update user data
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("key", sp.getString("apiKey"));
        params.put("profileImage", sp.getString("profileImage"));

        if (displayName.trim().length() > 0) {
            params.put("displayName", displayName.trim());
        }

        ActivityUtility.alertBox(EditProfileActivity.this, displayName.trim());

        WebManager.request(WebManager.API_URL + "api/account", WebManager.RequestType.PUT, params, new WebManager.RequestListener() {
            @Override
            public void onResponse(WebManager.ResponseObject response) {
                LayoutUtility.toggleActivityLayout(findViewById(R.id.constraintLayout), true);
                LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), false);

                if (response.getBoolean("success")){
                    ActivityUtility.toast(EditProfileActivity.this, response.getString("message"), 45);
                    return;
                }

                ActivityUtility.toast(EditProfileActivity.this, response.getString("message"), 45);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            ActivityUtility.startActivity(EditProfileActivity.this, SettingActivity.class);
            finish();
        }

        return false;
    }

}

