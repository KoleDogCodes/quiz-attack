package me.kolade.quizattack.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.kolade.quizattack.R;
import me.kolade.quizattack.adapter.FeedItem;
import me.kolade.quizattack.adapter.FeedItemAdapter;
import me.kolade.quizattack.handler.SocketManager;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.handler.WebManager;
import me.kolade.quizattack.listeners.ActivityListeners;
import me.kolade.quizattack.listeners.SocketService;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.LayoutUtility;
import me.kolade.quizattack.utility.QuizUtility;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class FeedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        SharedPreferencesWrapper sp = new SharedPreferencesWrapper(this);

        //Set navbar selected item
        BottomNavigationView navBar = findViewById(R.id.navBottom);
        navBar.setSelectedItemId(R.id.menuItemFeed);

        //Start navbar listener
        ActivityListeners.bottomNavBarSelectListener(this);

        //Show loading screen
        LayoutUtility.toggleActivityLayout(((ConstraintLayout) findViewById(R.id.constraintLayout)), false);
        LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), true);

        //Setup recycler viewer
        final RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final List<FeedItem> feedItemList = new ArrayList<FeedItem>();
        FeedItemAdapter apt = new FeedItemAdapter(FeedActivity.this, feedItemList, new FeedItemAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position, String postId) {
                onLikeClickCallback(view, position, postId);
            }
        });

        recyclerView.setAdapter(apt);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        //Retrieve posts
        WebManager.requestGet(WebManager.API_URL + "api/feed?key=" + User.getLocalApiKey(this) + "&topicId=1", new WebManager.RequestListener() {
            @Override
            public void onResponse(WebManager.ResponseObject response) {
                try {
                    JSONArray posts = response.getJSONArray("posts");

                    for (int i = 0; i < posts.length(); i++) {
                        JSONObject obj = posts.getJSONObject(i);
                        JSONObject userData = User.getRemoteUserDetails(FeedActivity.this, obj.getString("owner_id"));
                        JSONObject postMetadata = new JSONObject(obj.getString("misc"));

                        Bitmap encodedImage = obj.getString("image") == null? null: LayoutUtility.decodeBase64(obj.getString("image"));
                        Bitmap encodedIcon = userData.getString("image") == null? null: LayoutUtility.decodeBase64(userData.getString("image"));

                        FeedItem feedItem = new FeedItem(obj.getString("_id"), obj.getString("owner_id"), userData.getString("displayName") + " posted in " + QuizUtility.getTopics(FeedActivity.this)[Integer.valueOf(obj.getString("topic_id"))], obj.getString("content"), encodedIcon, encodedImage);
                        feedItem = feedItem.setMetadata(postMetadata);

                        feedItemList.add(feedItem);
                    }

                    //Hide loading screen
                    LayoutUtility.toggleActivityLayout(((ConstraintLayout) findViewById(R.id.constraintLayout)), true);
                    LayoutUtility.setViewVisbility(findViewById(R.id.relativeLayout), false);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            ActivityUtility.startActivity(FeedActivity.this, MainActivity.class);
            finish();
        }

        return false;
    }

    public void onCreatePostClick(View view) {
        ActivityUtility.startActivity(this, FeedPostActivity.class);
        finish();
    }

    public void onLikeClickCallback(View view, int position, final String postId) {
        try {
            //Get items from row view (View view)
            RecyclerView recyclerView = findViewById(R.id.recyclerView);
            final ImageView btnLike = view.findViewById(R.id.btnLike);
            TextView lblLikes = view.findViewById(R.id.lblLikes);

            //Get data object for the particular row
            final FeedItem feedItem = ((FeedItemAdapter) recyclerView.getAdapter()).getItem(position);
            boolean isLiked = false;

            //Fetch cached post metadata
            FeedItemAdapter adapter = (FeedItemAdapter) recyclerView.getAdapter();
            JSONObject misc = adapter.getItem(position).getPostMetadata();

            //Check if user is in like list and set likes status locally
            if (misc.getString("likes").contains(User.getLocalUserId(this))) {
                misc.put("likes", misc.getString("likes").replace(User.getLocalUserId(this) + ",", "").trim());
                btnLike.setImageResource(R.drawable.ic_thumb_up_black_24dp);
            }
            else {
                misc.put("likes", misc.getString("likes") + User.getLocalUserId(this) + ",");
                btnLike.setImageResource(R.drawable.ic_thumb_up_purple_24dp);
                isLiked = true;
            }

            int likes = misc.getString("likes").equals("") ? 0: (misc.getString("likes").split("\\,").length - 0);
            lblLikes.setText(String.valueOf(likes));

            //Add like to post
            Map<String, String> params = new HashMap<String, String>();
            params.put("key", User.getLocalApiKey(this));
            params.put("postId", postId);
            params.put("misc", misc.toString());

            final boolean finalIsLiked = isLiked;
            WebManager.request(WebManager.API_URL + "api/feed", WebManager.RequestType.PUT, params, new WebManager.RequestListener() {
                @Override
                public void onResponse(WebManager.ResponseObject response) {
                    try {
                        if (response.getBoolean("success")) {

                            //Tell owner of post, post was liked
                            if (finalIsLiked){
                                JSONObject obj = new JSONObject();
                                obj.put("type", SocketManager.MessageType.POST_STATUS_UPDATE);
                                obj.put("message", User.getLocalDisplayName(FeedActivity.this) + " has liked your post!");
                                obj.put("postId", postId);
                                obj.put("senderId", User.getLocalUserId(FeedActivity.this));
                                SocketManager.sendMessage("U_" + feedItem.getOwnerId(), obj);
                                ActivityUtility.toast(FeedActivity.this, "Post status has been updated!", 25);
                            }

                            return;
                        }

                        ActivityUtility.toast(FeedActivity.this, "Failed to like post.", 25);
                    }
                    catch (Exception e) {}
                }
            });

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}

