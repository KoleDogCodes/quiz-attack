package me.kolade.quizattack.handler;

import android.content.Context;

import org.json.JSONObject;

import java.util.HashMap;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import me.kolade.quizattack.listeners.SocketService;
import me.kolade.quizattack.utility.ActivityUtility;
import me.kolade.quizattack.utility.NotificationUtility;

public class SocketManager {

    private static Socket socket;
    private static HashMap<String, Boolean> tracker = new HashMap<>();

    public enum MessageType {
        POST_STATUS_UPDATE
    }

    public interface SocketResponse {
        void onResponse(JSONObject obj);
    }

    public static void setupSocket(final Context ctx) {
        if (tracker.containsKey("setup")) { return; }

        try {
            IO.Options opts = new IO.Options();
            opts.forceNew = false;
            opts.reconnection = true;

            socket = IO.socket("https://quiz-attack-socket.herokuapp.com/", opts);

            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    tracker.put("setup", true);
                    ActivityUtility.toast(ctx, "Successfully connected to the socket server.", 45);

                }

            });

            socket.connect();
        }
        catch (Exception e) { e.printStackTrace(); }
    }

    public static void addEventListener(String event, final SocketResponse sr) {
        if (tracker.containsKey(event)) { return; }

        socket.on(event, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                sr.onResponse((JSONObject) args[0]);
            }
        });

        tracker.put(event, true);
    }

    public static void sendMessage(String event, JSONObject obj) {
        socket.emit(event, obj);
    }

    public static void disconnectSocket() {
        socket.disconnect().close();
        tracker.clear();
    }

    public static void userListener(final Context ctx) {
        addEventListener("U_" + User.getLocalUserId(ctx), new SocketManager.SocketResponse() {
            @Override
            public void onResponse(JSONObject obj) {
                try {
                    //Ignore message if the sender id matches user id
                    if (obj.getString("senderId").equals(User.getLocalUserId(ctx))) { return; }

                    MessageType type = MessageType.valueOf(obj.getString("type").toUpperCase());

                    if (type == MessageType.POST_STATUS_UPDATE){
                        NotificationUtility.sendNotification(ctx, "QuizAttack", obj.getString("message"));
                    }

                }
                catch (Exception exc) { }
            }
        });
    }



}
