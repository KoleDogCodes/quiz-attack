package me.kolade.quizattack.handler;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class WebManager {

    public WebManager() {

    }

    public final static String API_URL = "https://quiz-attack-api.herokuapp.com/";

    public static class ResponseObject {

        private JSONObject obj;

        public ResponseObject(JSONObject obj) {
            this.obj = obj;
        }

        public Object get(String key) {
            try { return obj.get(key); }
            catch (Exception exc) { return null; }
        }

        public boolean getBoolean(String key) {
            try { return obj.getBoolean(key); }
            catch (Exception exc) { return false; }
        }

        public String getString(String key) {
            try { return obj.getString(key); }
            catch (Exception exc) { return null; }
        }

        public int getInt(String key) {
            try { return obj.getInt(key); }
            catch (Exception exc) { return 0; }
        }

        public double getDouble(String key) {
            try { return obj.getDouble(key); }
            catch (Exception exc) { return 0; }
        }

        public JSONArray getJSONArray(String key) {
            try { return obj.getJSONArray(key); }
            catch (Exception exc) { return null; }
        }

        public JSONObject getJSONObject(String key) {
            try { return obj.getJSONObject(key); }
            catch (Exception exc) { return null; }
        }

        @Override
        public String toString(){
            return obj.toString();
        }
    }

    public static void requestGet(final String targetURL, final RequestListener rl) {

        final Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    JSONObject object;

                    //Connect to url and post
                    URL url = new URL(targetURL);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");

                    //Read from inputstream
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String respLine = null;

                    while((respLine = reader.readLine()) != null){
                        response.append(respLine.trim());
                    }

                    reader.close();

                    object = new JSONObject(response.toString().trim());

                    rl.onResponse(new ResponseObject(object));

                } catch (Exception e) {
                    rl.onResponse(new ResponseObject(new JSONObject()));
                }
            }
        });

        thread.start();
    }

    public static Thread syncRequestGet(final String targetURL, final RequestListener rl) {

        final Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    JSONObject object;

                    //Connect to url and post
                    URL url = new URL(targetURL);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");

                    //Read from inputstream
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String respLine = null;

                    while((respLine = reader.readLine()) != null){
                        response.append(respLine.trim());
                    }

                    reader.close();

                    object = new JSONObject(response.toString().trim());

                    rl.onResponse(new ResponseObject(object));

                } catch (Exception e) {
                    rl.onResponse(new ResponseObject(new JSONObject()));
                }
            }
        });

        return thread;
    }

    public static void request(final String targetURL, final RequestType type, final Map<?, ?> params, final RequestListener rl) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //Generate json params from map
                    JSONObject postBody = new JSONObject();
                    for (Map.Entry<?, ?> entry : params.entrySet()) {
                        postBody.put(entry.getKey().toString(), entry.getValue());
                    }

                    //Connect to url set content heders
                    URL url = new URL(targetURL);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod(type.toString());
                    conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setDoOutput(true);

                    //Write json string to outputstream
                    OutputStream outStream = conn.getOutputStream();
                    byte[] input = postBody.toString().getBytes("UTF-8");
                    outStream.write(input);

                    //Read from inputstream
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String respLine = null;

                    while((respLine = reader.readLine()) != null){
                        response.append(respLine.trim());
                    }

                    reader.close();
                    outStream.close();
                    conn.disconnect();

                    JSONObject object = new JSONObject(response.toString().trim());

                    rl.onResponse(new ResponseObject(object));
                }
                catch (Exception exc) {
                    rl.onResponse(new ResponseObject(new JSONObject()));
                }
            }
        });

        thread.start();
    }

    public interface RequestListener {
        void onResponse(ResponseObject response);
    }

    public enum RequestType {
        POST,
        PUT,
        DELETE
    }
}

