package me.kolade.quizattack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import me.kolade.quizattack.R;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.utility.ActivityUtility;

public class FeedItemAdapter extends RecyclerView.Adapter<FeedItemAdapter.FeedViewHolder> {

    private Context ctx;
    private List<FeedItem> feedItemList;
    private ClickListener clickListener;

    public FeedItemAdapter(Context ctx, List<FeedItem> list, ClickListener clickListener){
        this.ctx = ctx;
        this.feedItemList = list;
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        public void onClick(View v, int position, String postId);
    }

    public class FeedViewHolder extends RecyclerView.ViewHolder {

        //Hold the content
        public ImageView imgIcon, imgContent, btnLike;
        public TextView lblHeader, lblContent, lblLikes;

        public FeedViewHolder(View view) {
            super(view);
            imgIcon = view.findViewById(R.id.imgIcon);
            imgContent = view.findViewById(R.id.imgContent);
            lblHeader = view.findViewById(R.id.lblHeader);
            lblContent = view.findViewById(R.id.lblContent);
            lblLikes = view.findViewById(R.id.lblLikes);
            btnLike = view.findViewById(R.id.btnLike);
        }

    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(ctx).inflate(R.layout.row_feed_item, parent, false);
        return new FeedItemAdapter.FeedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder holder, final int position) {
        try {
            holder.lblContent.setText(feedItemList.get(position).getContent());
            holder.lblHeader.setText(feedItemList.get(position).getHeader());

            int likes = feedItemList.get(position).getPostMetadata().getString("likes").equals("") ? 0: (feedItemList.get(position).getPostMetadata().getString("likes").split("\\,").length - 0);

            holder.lblLikes.setText(String.valueOf(likes));

            //Set content image
            if (feedItemList.get(position).getContentImage() != null) {
                holder.imgContent.setVisibility(View.VISIBLE);
                holder.imgContent.setImageBitmap(feedItemList.get(position).getContentImage());
            }

            //Set owner image image
            if (feedItemList.get(position).getOwnerImage() != null) {
                holder.imgIcon.setVisibility(View.VISIBLE);
                holder.imgIcon.setImageBitmap(feedItemList.get(position).getOwnerImage());
            }

            //Set like icon status
            if (feedItemList.get(position).getPostMetadata().getString("likes").contains(User.getLocalUserId(holder.itemView.getContext()))) {
                holder.btnLike.setImageResource(R.drawable.ic_thumb_up_purple_24dp);
            }

            //Set tags
            holder.itemView.findViewById(R.id.btnLike).setTag(position);
            holder.itemView.setTag(feedItemList.get(position).getPostId());

            final View rowView = holder.itemView.getRootView();

            holder.btnLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClick(rowView, position, feedItemList.get(position).getPostId());
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return feedItemList.size();
    }

    public FeedItem getItem(int position) {
        return feedItemList.get(position);
    }

}

