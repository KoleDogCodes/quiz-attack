package me.kolade.quizattack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import me.kolade.quizattack.R;

public class QuizIconItemAdapter extends RecyclerView.Adapter<QuizIconItemAdapter.QuizItemIconViewHolder> {

    private Context ctx;
    private List<QuizIconItem> itemList;
    private InnerClickListener cl;

    public QuizIconItemAdapter(Context ctx, List<QuizIconItem> list, InnerClickListener cl){
        this.ctx = ctx;
        this.itemList = list;
        this.cl = cl;
    }

    public class QuizItemIconViewHolder extends RecyclerView.ViewHolder {

        //Hold the content
        public TextView lblTitle, lblCategory;
        public ImageView imgIcon;

        public QuizItemIconViewHolder(View view) {
            super(view);
            lblTitle = view.findViewById(R.id.lblTitle);
            lblCategory = view.findViewById(R.id.lblCategory);
            imgIcon = view.findViewById(R.id.imgIcon);
        }

    }

    public interface InnerClickListener {
        void onClick(View v, String quizId, int position);
    }

    @NonNull
    @Override
    public QuizItemIconViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(ctx).inflate(R.layout.row_quiz_icon_item, parent, false);
        return new QuizIconItemAdapter.QuizItemIconViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull QuizItemIconViewHolder holder, final int position) {
        holder.lblTitle.setText(itemList.get(position).getTitle());
        holder.lblCategory.setText(itemList.get(position).getTopic());
        holder.imgIcon.setImageBitmap(itemList.get(position).getIcon());

        final View view = holder.itemView;

        holder.imgIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cl.onClick(view, itemList.get(position).getId(), position);
            }
        });

        holder.lblCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cl.onClick(view, itemList.get(position).getId(), position);
            }
        });

        holder.lblTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cl.onClick(view, itemList.get(position).getId(), position);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public QuizIconItem getItem(int position) {
        return itemList.get(position);
    }

}

