package me.kolade.quizattack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import me.kolade.quizattack.R;


public class QuestionResultsAdapter extends ArrayAdapter<QuestionResultItem> {

    private List<QuestionResultItem> items;
    private Context ctx;

    public QuestionResultsAdapter(List<QuestionResultItem> items, Context context, int resource) {
        super(context, resource, items);

        this.items = items;
        this.ctx = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        QuestionResultItem item = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_question_result, parent, false);
        }

        //Get the views from the custom view
        ImageView imgResult = convertView.findViewById(R.id.imgResult);
        TextView lblHeader = convertView.findViewById(R.id.lblHeader);

        lblHeader.setText("Question " + (item.getQuestionNumber() + 1));

        if (item.isCorrect()) {
            imgResult.setImageResource(R.drawable.ic_check_green_24dp);
        }
        else {
            imgResult.setImageResource(R.drawable.ic_clear_red_24dp);
        }

        return convertView;
    }
}
