package me.kolade.quizattack.adapter;

import android.graphics.Bitmap;

import org.json.JSONObject;

public class FeedItem {

    private Bitmap icon, contentImage;
    private String header, content, id, ownerId;
    private JSONObject obj;

    public FeedItem(String postId, String ownerId, String header, String content, Bitmap icon, Bitmap contentImage) {
        this.header = header;
        this.content = content;
        this.icon = icon;
        this.contentImage = contentImage;
        this.id = postId;
        this.ownerId = ownerId;
    }

    public String getPostId() { return id; }

    public String getOwnerId() {
        return ownerId;
    }

    public Bitmap getOwnerImage() {
        return icon;
    }

    public Bitmap getContentImage() {
        return contentImage;
    }

    public String getHeader() {
        return header;
    }

    public String getContent() { return content; }

    public FeedItem setMetadata(JSONObject obj) {
        this.obj = obj;
        return this;
    }

    public JSONObject getPostMetadata() { return obj; }
}
