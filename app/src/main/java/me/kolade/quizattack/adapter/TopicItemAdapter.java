package me.kolade.quizattack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import me.kolade.quizattack.R;
import me.kolade.quizattack.handler.User;

public class TopicItemAdapter extends RecyclerView.Adapter<TopicItemAdapter.TopicViewHolder> {

    private Context ctx;
    private List<TopicItem> itemList;
    private ClickListener cl;

    public TopicItemAdapter(Context ctx, List<TopicItem> list, ClickListener cl){
        this.ctx = ctx;
        this.itemList = list;
        this.cl = cl;
    }

    public class TopicViewHolder extends RecyclerView.ViewHolder {

        //Hold the content
        public TextView lblTitle;

        public TopicViewHolder(View view) {
            super(view);
            lblTitle = view.findViewById(R.id.lblHeader);
        }
    }

    public interface ClickListener {
        void onClick(View v, String quizId, int position);
    }

    @NonNull
    @Override
    public TopicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(ctx).inflate(R.layout.row_topic_item, parent, false);
        return new TopicItemAdapter.TopicViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TopicViewHolder holder, final int position) {
        holder.lblTitle.setText(itemList.get(position).getTitle());

        //Setup sub recycler viewer
        RecyclerView recyclerView = holder.itemView.findViewById(R.id.recyclerView);

        //Pass event from inner recycler to primary
        QuizIconItemAdapter adpt = new QuizIconItemAdapter(holder.itemView.getContext(), itemList.get(position).getIcons(), new QuizIconItemAdapter.InnerClickListener() {
            @Override
            public void onClick(View v, String quizId, int position) {
                cl.onClick(v, quizId, position);
            }
        });

        recyclerView.setAdapter(adpt);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(holder.itemView.getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public TopicItem getItem(int position) {
        return itemList.get(position);
    }

}

