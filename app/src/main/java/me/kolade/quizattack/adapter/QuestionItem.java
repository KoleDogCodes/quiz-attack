package me.kolade.quizattack.adapter;

import android.graphics.Bitmap;

public class QuestionItem {

    private int id, checked;
    private String question;
    private String[] options;
    private Bitmap bmp;

    public QuestionItem(int number, String question, String[] options, int checked) {
        //Basic Validation
        if (checked > 4 || checked < 0) { checked = 0; }
        if (options == null){ options = new String[]{"", "", "", ""}; }

        this.question = question;
        this.id = number;
        this.options = options;
        this.checked = checked;
    }

    public int getQuestionNumber() {
        return id;
    }

    public int getCorrectQuestion() {
        return checked;
    }

    public String getQuestion() {
        return question;
    }

    public String[] getOptions() {
        return options;
    }

    public Bitmap getQuestionImage() {
        return bmp;
    }

    public QuestionItem setImage(Bitmap bmp) {
        this.bmp = bmp;
        return this;
    }
}
