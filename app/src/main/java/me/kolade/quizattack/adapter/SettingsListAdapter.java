package me.kolade.quizattack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.kolade.quizattack.R;


public class SettingsListAdapter extends ArrayAdapter<SettingItem> {

    private List<SettingItem> items;
    private Context ctx;

    public SettingsListAdapter(List<SettingItem> items, Context context, int resource) {
        super(context, resource, items);

        this.items = items;
        this.ctx = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SettingItem item = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_setting_item, parent, false);
        }

        //Get the views from the custom view
        ImageView imgIcon = convertView.findViewById(R.id.imgRowIcon);
        TextView txtDesc = convertView.findViewById(R.id.txtRowDesc);
        EditText txtInput = convertView.findViewById(R.id.txtRowInput);
        Switch btnToggle = convertView.findViewById(R.id.btnRowToggle);
        ImageView imgNext = convertView.findViewById(R.id.imgRowNext);

        //Get what type of row should be inputted
        SettingItem.Type type = item.getType();

        //Set icon to profile picture
        if (item.getBitmap() == null) {
            imgIcon.setImageResource(item.getImage());
        }
        else {
            imgIcon.setImageBitmap(item.getBitmap());
        }

        txtDesc.setText(item.getDesc());
        btnToggle.setChecked(item.isChecked());
        txtInput.setHint(item.getDesc());
        txtInput.setText(item.getPrefillText());

        //Toggle visibility of view depending on what type is selected
        if (type == SettingItem.Type.NEXT) {
            txtInput.setVisibility(View.GONE);
            btnToggle.setVisibility(View.GONE);

            txtDesc.setVisibility(View.VISIBLE);
            imgNext.setVisibility(View.VISIBLE);
        }
        else if (type == SettingItem.Type.SWITCH) {
            txtInput.setVisibility(View.GONE);
            imgNext.setVisibility(View.GONE);

            btnToggle.setVisibility(View.VISIBLE);
        }
        else if (type == SettingItem.Type.TEXT_FIELD) {
            imgNext.setVisibility(View.GONE);
            txtDesc.setVisibility(View.GONE);
            btnToggle.setVisibility(View.GONE);

            txtInput.setVisibility(View.VISIBLE);
        }
        else {
            btnToggle.setVisibility(View.GONE);
            imgNext.setVisibility(View.GONE);
            txtInput.setVisibility(View.GONE);
        }

        return convertView;
    }
}
