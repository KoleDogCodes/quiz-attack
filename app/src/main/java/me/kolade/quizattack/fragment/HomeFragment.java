package me.kolade.quizattack.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import me.kolade.quizattack.R;

public class HomeFragment extends Fragment {

    private View view;

    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_home, null);

//        BottomNavigationView navMenu = this.view.findViewById(R.id.navHome);
//        navMenu.setSelectedItemId(R.id.menuItemHolder);

        return this.view;
    }
}
