package me.kolade.quizattack.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import me.kolade.quizattack.R;

public class HomeStatsFragment extends Fragment {

    private View view;

    public HomeStatsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //this.view = inflater.inflate(R.layout.fragment_home, null);
        return inflater.inflate(R.layout.fragment_home_stats, container, false);

        //return this.view;
    }
}
