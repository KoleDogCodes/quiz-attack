package me.kolade.quizattack.wrapper;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesWrapper {

    private SharedPreferences sh;

    public SharedPreferencesWrapper(Context cxt){
        this.sh = cxt.getSharedPreferences("QuizAttack", Context.MODE_PRIVATE);
    }

    public boolean addValue(String key, Object value) {
        SharedPreferences.Editor shEdit = sh.edit();

        if (value instanceof Long) {
            shEdit.putLong(key, (long) value);
        }
        else if (value instanceof String) {
            shEdit.putString(key, value.toString());
        }
        else if (value instanceof Integer){
            shEdit.putInt(key, (int) value);
        }
        else if (value instanceof Boolean){
            shEdit.putBoolean(key, (boolean) value);
        }
        else if (value instanceof Float){
            shEdit.putFloat(key, (float) value);
        }
        else {
            return false;
        }

        return  shEdit.commit();
    }

    public boolean hasKey(String key) {
        return sh.contains(key);
    }

    public long getLong(String key){
        return sh.getLong(key, 0);
    }

    public String getString(String key){
        return sh.getString(key, null);
    }

    public int getInt(String key){
        return sh.getInt(key, 0);
    }

    public boolean getBoolean(String key){
        return sh.getBoolean(key, false);
    }

    public float getFloat(String key){
        return sh.getFloat(key, 0.0f);
    }

    public boolean removeKey(String key) {
        SharedPreferences.Editor shEdit = sh.edit();
        shEdit = shEdit.remove(key);
        shEdit.commit();
        return hasKey(key);
    }

    public boolean clear() {
        SharedPreferences.Editor shEdit = sh.edit();
        return shEdit.clear().commit();
    }

}