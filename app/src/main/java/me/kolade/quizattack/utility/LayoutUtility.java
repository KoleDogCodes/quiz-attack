package me.kolade.quizattack.utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.text.Layout;
import android.text.Spanned;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.kolade.quizattack.R;

public class LayoutUtility {

    public LayoutUtility() {

    }

    public static void toggleActivityLayout(final Object obj, final boolean toggle) {
        Handler handler = new Handler(Looper.getMainLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (obj instanceof ConstraintLayout){
                    ConstraintLayout layout = (ConstraintLayout) obj;
                    for (int i = 0; i < layout.getChildCount();  i++){
                        View v = layout.getChildAt(i);
                        v.setEnabled(toggle);
                        v.setClickable(toggle);
                    }
                }
                else if (obj instanceof RelativeLayout){
                    RelativeLayout layout = (RelativeLayout) obj;
                    for (int i = 0; i < layout.getChildCount();  i++){
                        View v = layout.getChildAt(i);
                        v.setEnabled(toggle);
                        v.setClickable(toggle);
                    }
                }
            }
        });
    }

    public static void setViewVisbility(final View v, final boolean visible) {
        Handler handler = new Handler(Looper.getMainLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {
            if (visible) {
                v.setVisibility(View.VISIBLE);
            }
            else {
                v.setVisibility(View.GONE);
            }
            }
        });
    }

    public static void runMethod(final Class cl, final Object v, final String name, final Object... args) {
        Handler handler = new Handler(Looper.getMainLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Method> methods = new ArrayList<Method>();
                    methods.addAll(Arrays.asList(cl.getDeclaredMethods()));
                    methods.addAll(Arrays.asList(cl.getMethods()));

                    for (Method method: methods) {
                        try {
                            if (method.getName().equalsIgnoreCase(name)){
                                method.invoke(cl.cast(v), args);
                                break;
                            }
                        }
                        catch (Exception e){ continue; }
                    }
                }
                catch (Exception e) { e.printStackTrace(); }
            }
        });
    }

    public static void runViewMethod(final View v, final String name, final Object... args) {
        Handler handler = new Handler(Looper.getMainLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    Class cl = v.getClass();

                    List<Method> methods = new ArrayList<Method>();
                    methods.addAll(Arrays.asList(cl.getDeclaredMethods()));
                    methods.addAll(Arrays.asList(cl.getMethods()));

                    for (Method method: methods) {
                        try {
                            if (method.getName().equalsIgnoreCase(name)){
                                method.invoke(cl.cast(v), args);
                                break;
                            }
                        }
                        catch (Exception e){ continue; }
                    }
                }
                catch (Exception e) { e.printStackTrace(); }
            }
        });
    }

    public static String encodeBitmap(Bitmap bitmap){
        if (bitmap == null) { return ""; }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byte_arr = stream.toByteArray();
        return Base64.encodeToString(byte_arr, Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String encodedText) {
        if (encodedText.trim().equalsIgnoreCase("")) { return null; }

        String realEncodedText = encodedText.substring(encodedText.indexOf(",")  + 1);
        byte[] decodedBytes = Base64.decode(realEncodedText, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        return bitmap;
    }
}
