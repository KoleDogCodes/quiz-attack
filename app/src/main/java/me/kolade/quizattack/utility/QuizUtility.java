package me.kolade.quizattack.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import me.kolade.quizattack.activity.TopicActivity;
import me.kolade.quizattack.adapter.QuestionItem;
import me.kolade.quizattack.adapter.QuizIconItem;
import me.kolade.quizattack.handler.Quiz;
import me.kolade.quizattack.handler.User;
import me.kolade.quizattack.handler.WebManager;
import me.kolade.quizattack.wrapper.SharedPreferencesWrapper;

public class QuizUtility {

    public QuizUtility() {

    }

    public static String[] getTopics(Context ctx) {
        try {
            SharedPreferencesWrapper sp = new SharedPreferencesWrapper(ctx);

            JSONArray topics = new JSONArray(sp.getString("topics"));
            List<String> strTopics = new ArrayList<String>();

            //Populate primary recycle viewer
            for (int i = 0; i < topics.length(); i++) {
                JSONObject topic = topics.getJSONObject(i);
                strTopics.add(topic.getString("name"));
            }

            return strTopics.toArray(new String[0]);
        }
        catch (Exception e) {
            e.printStackTrace();
            return new String[]{};
        }
    }

    public static List<QuizIconItem> getQuizzes(final Context ctx, int topicID) {
        final List<QuizIconItem> result = new ArrayList<QuizIconItem>();

        try {
            Thread thread = WebManager.syncRequestGet(WebManager.API_URL + "api/quiz?key=" + User.getLocalApiKey(ctx) + "&topicId=" + topicID, new WebManager.RequestListener() {
                @Override
                public void onResponse(WebManager.ResponseObject response) {
                    try {
                        JSONArray array = new JSONArray(response.getString("data"));

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);

                            String id = obj.getString("_id");
                            String title = obj.getString("title");
                            String topic = QuizUtility.getTopics(ctx)[Integer.valueOf(obj.getString("topic_id"))];
                            Bitmap img = LayoutUtility.decodeBase64(obj.getString("icon"));

                            result.add(new QuizIconItem(id, title, topic, img));
                        }

                        return;
                    }
                    catch (Exception e) { e.printStackTrace(); }
                }
            });

            thread.start();
            thread.join();
            return result;
        }
        catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    public static List<QuizIconItem> getCreatedQuizzes(final Context ctx) {
        final List<QuizIconItem> result = new ArrayList<QuizIconItem>();

        try {
            Thread thread = WebManager.syncRequestGet(WebManager.API_URL + "api/user/quiz?key=" + User.getLocalApiKey(ctx), new WebManager.RequestListener() {
                @Override
                public void onResponse(WebManager.ResponseObject response) {
                    try {
                        JSONArray array = new JSONArray(response.getString("data"));

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);

                            String id = obj.getString("_id");
                            String title = obj.getString("title");
                            String topic = QuizUtility.getTopics(ctx)[Integer.valueOf(obj.getString("topic_id"))];
                            Bitmap img = LayoutUtility.decodeBase64(obj.getString("icon"));

                            result.add(new QuizIconItem(id, title, topic, img));
                        }

                        return;
                    }
                    catch (Exception e) { e.printStackTrace(); }
                }
            });

            thread.start();
            thread.join();
            return result;
        }
        catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    public static Quiz getQuiz(final Context ctx, final String quizId) {
        final AtomicReference<Quiz> quiz = new AtomicReference<Quiz>(null);

        try {
            Thread thread = WebManager.syncRequestGet(WebManager.API_URL + "api/quiz/" + quizId.trim() + "?key=" + User.getLocalApiKey(ctx), new WebManager.RequestListener() {
                @Override
                public void onResponse(WebManager.ResponseObject response) {
                    try {
                        JSONArray array = response.getJSONArray("data");

                        JSONObject obj = array.getJSONObject(0);

                        List<QuestionItem> questionList = new ArrayList<QuestionItem>();

                        String id = obj.getString("_id");
                        String title = obj.getString("title");
                        String topicId = obj.getString("topic_id");
                        Bitmap img = LayoutUtility.decodeBase64(obj.getString("icon"));

                        //Deserialize questions from json
                        JSONArray jsonQuestions = new JSONArray(obj.getString("metadata"));

                        for (int i = 0; i < jsonQuestions.length(); i++) {
                            JSONObject jsonQuestion = new JSONObject(jsonQuestions.getString(i));

                            Log.wtf("[WEB MANAGER]", jsonQuestion.toString());

                            String question = jsonQuestion.getString("question");
                            int correctQuestion = jsonQuestion.getInt("answer");
                            Bitmap questionImage = LayoutUtility.decodeBase64(jsonQuestion.getString("image"));

                            QuestionItem item = new QuestionItem(i, question, toArray(new JSONArray(jsonQuestion.getString("options"))), correctQuestion).setImage(questionImage);
                            questionList.add(item);
                        }

                        //Set quiz data object
                        Quiz localQuiz = new Quiz(id, title, topicId);
                        localQuiz.setIcon(img);
                        localQuiz.setQuestions(questionList);

                        quiz.set(localQuiz);

                        return;
                    }
                    catch (Exception e) { e.printStackTrace(); }
                }
            });

            thread.start();
            thread.join();
            return quiz.get();
        }
        catch (Exception e) {
            e.printStackTrace();
            return quiz.get();
        }
    }

    public static String[] toArray(JSONArray array) {
        String[] result = new String[array.length()];

        try {
            for (int i = 0; i < array.length(); i++) {
                result[i] = array.get(i).toString();
            }

            return  result;
        }
        catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }
}
