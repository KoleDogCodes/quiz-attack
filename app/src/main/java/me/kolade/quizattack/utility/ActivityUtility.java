package me.kolade.quizattack.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.EditText;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import me.kolade.quizattack.R;
import me.kolade.quizattack.fragment.LoadingScreenFragment;
import me.kolade.quizattack.fragment.MainLoadingScreenFragment;
import me.kolade.quizattack.handler.InputDialog;

public class ActivityUtility {

    public ActivityUtility() {

    }

    private static Map<String, Fragment> fragmentMap = new HashMap<String, Fragment>();

    public static void startActivity(final Context cxt, final Class<?> newActivity) {
        Handler handler = new Handler(Looper.getMainLooper());
        final Activity activity = (Activity) cxt;

        handler.post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(cxt, newActivity);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                cxt.startActivity(intent);
                activity.finish();
            }
        });
    }

    public static void startActivity(final Context cxt, final Class<?> newActivity, final Intent extras) {
        Handler handler = new Handler(Looper.getMainLooper());
        final Activity activity = (Activity) cxt;

        handler.post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(cxt, newActivity);
                intent.putExtras(extras);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                cxt.startActivity(intent);
                activity.finish();
            }
        });
    }

    public static void startActivity(final Context cxt, final Class<?> newActivity, final String key, final String value) {
        Handler handler = new Handler(Looper.getMainLooper());
        final Activity activity = (Activity) cxt;

        handler.post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(cxt, newActivity);
                intent.putExtra(key, value);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                cxt.startActivity(intent);
                activity.finish();
            }
        });
    }

    public static void addFragment(FragmentManager fm, int id, Fragment frag) {
        FragmentTransaction trans = fm.beginTransaction();
        trans.add(id, frag, frag.getClass().getSimpleName());
        trans.commit();
    }

    public static boolean showFragment(FragmentManager fm, Class cl) {
        FragmentTransaction trans = fm.beginTransaction();

        for (Fragment f: fm.getFragments()) {
            if (f.getClass().getSimpleName().equalsIgnoreCase(cl.getSimpleName())) {
                trans.show(f).commit();
                return true;
            }
        }

        return false;
    }

    public static void hideFragment(FragmentManager fm, Class cl) {
        FragmentTransaction trans = fm.beginTransaction();

        for (Fragment f: fm.getFragments()) {
            if (f.getClass().getSimpleName().equalsIgnoreCase(cl.getSimpleName())){
                trans.hide(f);
                break;
            }
        }

        trans.commit();
    }

    public static boolean showOneFragment(FragmentManager fm, Class cl) {
        FragmentTransaction trans = fm.beginTransaction();
        boolean result = false;

        for (Fragment f: fm.getFragments()) {
            if (f.getClass().getSimpleName().equalsIgnoreCase(cl.getSimpleName())){
                trans.show(f).commit();
                result = true;
                continue;
            }

//            trans.hide(f);
        }

//        trans.commit();
        return result;
    }

    public static void alertBox(final Context ctx, final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                AlertDialog alert = new AlertDialog.Builder(ctx).create();
                alert.setTitle("Quiz Attack");
                alert.setMessage(message);
                alert.show();
            }
        });
    }

    public static void showUnCancelableDialog(final Context ctx, final String title, final CharSequence[] options, final DialogListener dl) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
                dialog.setTitle(title);
                dialog.setCancelable(false);
                dialog.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dl.onClick(dialog ,which);
                    }
                });

                dialog.show();
            }
        });
    }

    public static void showDialog(final Context ctx, final String title, final CharSequence[] options, final DialogListener dl) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
                dialog.setTitle(title);
                dialog.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dl.onClick(dialog ,which);
                    }
                });

                dialog.show();
            }
        });
    }

    public static void showDialog(final Context ctx, final String title, final InputDialog.DialogResult dr) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                InputDialog dialog = new InputDialog(ctx, title);
                dialog.show();
                dialog.setSuccessListener(new InputDialog.DialogResult() {
                    @Override
                    public void onSuccess(String result) {
                        dr.onSuccess(result);
                    }
                });
            }
        });
    }

    public static void toast(final Context ctx, final String message, final int duration) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(ctx, message, Toast.LENGTH_LONG);
                toast.setDuration(duration);
                toast.setText(message);
                toast.show();
            }
        });
    }

    public static void runSyncTask(final SyncTask task){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                task.run();
            }
        });
    }

    public static Thread runAsyncTask(Runnable run) {
        Thread thread = new Thread(run);
        thread.start();
        return thread;
    }

    public interface DialogListener {
        void onClick(DialogInterface dialog, int position);
    }

    public interface DialogResult {
        void onSuccess(DialogInterface dialog, String result);
    }

    public interface SyncTask {
        void run();
    }
}
